function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local position = parameter.position -- Vec3
	local radius = parameter.radius -- array of Vec3

	Spring.GiveOrder(CMD.UNLOAD_UNIT,{position.x, position.y, position.z, radius},{}) 
	
	return SUCCSESS
end