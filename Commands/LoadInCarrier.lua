function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "radius", -- relative radius
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local position = parameter.position -- Vec3
	local radius = parameter.radius -- array of Vec3

	local team = Spring.GetLocalTeamID()
    local ally_units = Spring.GetUnitsInCylinder (position.x, position.z, radius, team)
	
	Spring.GiveOrder(CMD.LOAD_UNITS,{position.x, position.y, position.z, radius},{}) 

	local unitsInCarrier = Spring.GetUnitIsTransporting(units[1])
	
	if(#ally_units == #unitsInCarrier + 1)
		then return SUCCESS
		else return RUNNING
	end
end