function getInfo()
	return 
	{
		tooltip = "Move to defined position",
		parameterDefs = 
		{
			{ 
				name = "target_position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run (self, units, parameter)
	--all allied units
	local myTeamID = Spring.GetMyTeamID()
	local allMyUnits = Spring.GetTeamUnits(myTeamID)
	local points = parameter.target_position -- Vec3
	
	if (points~=nil and allMyUnits~=nil and Spring.IsGameOver()==false) 
	then
	
		local counter = #points --counter for the cycle through points
		
		if (counter>#allMyUnits) --if there are less units then points
		then 
			counter = #allMyUnits --counter is based on the number of the units
		end
		if (counter>0)
		then
			for i = 1, counter do
				Spring.GiveOrderToUnit(allMyUnits[i],10, {points[i].x,points[i].y,points[i].z},{""})
			end
		end
	end
	
	return RUNNING--Spring.IsGameOver()
end