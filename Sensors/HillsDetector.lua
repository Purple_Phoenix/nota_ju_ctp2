local sensorInfo = {
	name = "detectHill",
	desc = "Return the array of hills with desired or greater height.",
	author = "NotaUserB",
	date = "2019-04-21",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- this sensor is not caching any values, it evaluates itself on every request
local TILE_SIZE = 128 -- in elmos
local mapX = Game.mapSizeX 
local mapZ = Game.mapSizeZ
local mapTilesX = math.ceil(mapX / TILE_SIZE) 
local mapTilesZ = math.ceil(mapZ / TILE_SIZE)
local mapArraySize = mapTilesX * mapTilesZ

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(hillHeight)
	--unit position:
		local bx,by,bz = Spring.GetUnitPosition(units[1])
		local currentPosition = Vec3(bx, by, bz)	
		
	--future hill coordinates (at unit's coordinates at the beginning)	
		local hillX = bx 
		local hillZ = bz 
		local hillY = by 
		
	--array for hills' coordinates
		local hillArray = {}
		local ind = 1 
			
		local x = -TILE_SIZE
		local z = 0
	
		
		for i=1, mapArraySize do	
			if (i % mapTilesZ == 1) then
				z = 0
				x = x + TILE_SIZE
			else
				z = z + TILE_SIZE
			end
			
			
			hillX = x+TILE_SIZE/2
			hillZ = z+TILE_SIZE/2
			hillY = Spring.GetGroundHeight(hillX,hillZ)
			
			if (hillY>hillHeight) --if the height is acceptable
				then 
					hillArray[ind] = { --put the newfound hill into the array
						x = hillX,
						y = hillY,
						z = hillZ,
					}
					ind = ind+1
				end
			
		end
	
		
		
	return hillArray

end
